# Dogs

Android native demo app showing dogs on 2 screens
- A screen with a list of clickable breeds and sub-breeds
- A detail screen for the selected post with the images of the breed/sub-breed

The dogs data is fetched from Dog API
    `https://dog.ceo/dog-api/`

To make it running just import it to Android Studio Arctic Fox (2020.3.1 Beta 2) or
use ./gradlew assemble command to assemble the apk and install it on a device

![](./art/Screenshot_1.png)

TODO:
- write automated/UI tests
- improve unit tests