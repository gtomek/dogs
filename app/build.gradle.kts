
plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("io.gitlab.arturbosch.detekt")
}


android {
    compileSdk = Config.Android.compileSdkVersion
    buildToolsVersion = Config.Android.buildToolsVersion

    defaultConfig {
        applicationId = "uk.co.tomek.dogs"
        minSdk = Config.Android.minSdkVersion
        targetSdk = Config.Android.targetSdkVersion
        versionCode = Config.App.versionCode
        versionName = Config.App.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField("String", "SERVER_URL", "\"https://dog.ceo/api/\"")
        buildConfigField("String", "ACCESS_TOKEN", "\"${System.getenv("ACCESS_TOKEN")}\"")

        //https://www.giorgosneokleous.com/2019/12/01/name-your-apk-aab-files/
        setProperty("archivesBaseName", "dogsApp-v${versionName}(${versionCode})")
    }

    buildTypes {
        getByName("debug") {
            applicationIdSuffix = ".debug"
        }
        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
    }

}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar", "*.aar"))))
    implementation(kotlin("stdlib-jdk8", Config.kotlinVersion))
    implementation(Libraries.material)
    implementation(Libraries.androidxAppCompat)
    implementation(Libraries.androidxCoreKtx)
    implementation(Libraries.androidxConstraintLayout)
    implementation(Libraries.androidxNavigationFragment)
    implementation(Libraries.androidxNavigationUiKtx)
    implementation(Libraries.kotlinCoroutinesCore)
    implementation(Libraries.kotlinCoroutinesAndroid)
    implementation(Libraries.kotlinReflect) // to avoid kotlin versions discrepancy

    // dependency injection
    implementation(Libraries.koinCore)
    implementation(Libraries.koinAndroid)
    implementation(Libraries.koinAndroidViewModel)

    // logging library
    implementation(Libraries.timber)

    // images loading
    implementation(Libraries.coilImagesLoader)

    // networking
    implementation(Libraries.retrofit)
    implementation(Libraries.retrofitConverterMoshi)
    implementation(Libraries.okHttp)
    implementation(Libraries.okHttpTls)
    implementation(Libraries.okHttpUrlConnection)
    implementation(Libraries.okHttpLoggingInterceptor)
    implementation(Libraries.moshi)
    "debugImplementation"(Libraries.chuckDebug)
    "releaseImplementation"(Libraries.chuckRelease)

    // debugImplementation because LeakCanary should only run in debug builds.
    debugImplementation(Libraries.leakCanary)

    testImplementation(TestLibraries.junit4)
    testImplementation(TestLibraries.mockitoCore)
    testImplementation(TestLibraries.mockitoInline)
    testImplementation(TestLibraries.mockitoKotlin)
    testImplementation(TestLibraries.coroutinesTest)
    testImplementation(TestLibraries.androidXCoreTesting)

    androidTestImplementation(TestLibraries.testRunner)
    androidTestImplementation(TestLibraries.androidXTestJunit)
    androidTestImplementation(TestLibraries.espressoCore)
   // androidTestImplementation(TestLibraries.restMockServer)
    androidTestImplementation(TestLibraries.androidXTestRules)
}

detekt {
    toolVersion = "1.17.1"
    input = files(  // The directories where detekt looks for input files. Defaults to `files("src/main/java", "src/main/kotlin")`.
        "src/main/java",
        "src/test/java"
    )
    parallel = false  // Builds the AST in parallel. Rules are always executed in parallel. Can lead to speedups in larger projects. `false` by default.
    config = files("${rootProject.projectDir}/gradle/scripts/detekt-config.yml")
    ignoreFailures = false

    reports {
        txt {
            enabled = true // Enable/Disable TXT report (default: true)
            destination =
                file("build/reports/detekt.txt") // Path where TXT report will be stored (default: `build/reports/detekt/detekt.txt`)
        }
        xml {
            enabled = false
        }
        html {
            enabled = true                                // Enable/Disable HTML report (default: true)
            destination = file("build/reports/detekt.html")
        }
    }
    dependencies {
        detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.17.1")
    }
}

