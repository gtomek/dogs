package uk.co.tomek.dogs.domain

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import org.junit.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.tomek.dogs.data.DogsRepository
import uk.co.tomek.dogs.data.model.ImagesResponse
import uk.co.tomek.dogs.presentation.model.DetailsViewState
import java.lang.IllegalStateException

@ExperimentalCoroutinesApi
class DetailsInteractorTest {

    private lateinit var interactor: DetailsInteractor

    @Mock
    private lateinit var repository: DogsRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        interactor = DetailsInteractor(repository)
    }

    @Test
    fun verifyFetchPhotosWithSomeData() {
        runBlockingTest {
            // given
            val url1 = "url1"
            val url2 = "url2"
            val dogsResponse = ImagesResponse(listOf(url1, url2), "success")
            given(repository.fetchPhotos(any(), any())).willReturn(dogsResponse)
            val dogsPhotosList = listOf(url1, url2)
            val expected = DetailsViewState.Data(dogsPhotosList)

            // when
            val fetchedState = interactor.fetchData(url1)

            // then
            assertEquals(expected, fetchedState)
        }
    }

    @Test
    fun verifyFetchPhotosWithEmptyParaders() {
        runBlockingTest {
            // given
            val dogsResponse = ImagesResponse(emptyList(), "success")
            given(repository.fetchPhotos(any(), any())).willReturn(dogsResponse)
            val expected = DetailsViewState.Error()

            // when
            val fetchedState = interactor.fetchData(null)

            // then
            assertEquals(expected, fetchedState)
        }
    }

    @Test
    fun verifyFetchPhotosWithError() {
        runBlockingTest {
            // given
            val url1 = "url1"
            val error = IllegalStateException("An error!")
            given(repository.fetchPhotos(any(), any())).willThrow(error)
            val expected = DetailsViewState.Error(error)

            // when
            val fetchedState = interactor.fetchData(url1)

            // then
            assertEquals(expected, fetchedState)
        }
    }
}