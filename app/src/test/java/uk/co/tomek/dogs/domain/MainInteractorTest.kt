package uk.co.tomek.dogs.domain

import com.nhaarman.mockitokotlin2.given
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.tomek.dogs.data.DogsRepository
import uk.co.tomek.dogs.data.model.DogsResponse
import uk.co.tomek.dogs.presentation.model.DogsListItem
import uk.co.tomek.dogs.presentation.model.MainViewState

class MainInteractorTest {

    @Mock
    private lateinit var repository: DogsRepository
    private lateinit var interactor: MainInteractor
    private val mapper = DogsMapper()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        interactor = MainInteractor(
            repository,
            mapper
        )
    }

    @ExperimentalStdlibApi
    @Test
    fun verifyFetchData() {
        runBlocking {
            // given
            val breed1 = "springer"
            val subBreed = "english"
            val dogs = mapOf(breed1 to listOf(subBreed))
            val dogsResponse = DogsResponse(dogs, "success")
            given(repository.fetchDogs()).willReturn(dogsResponse)
            val dogsList = listOf(DogsListItem(breed1), DogsListItem(subBreed, true, breed1))
            val expected = MainViewState.Data(dogsList)

            // when
            val fetchedState = interactor.fetchData(null)

            // then
            assertEquals(expected, fetchedState)
        }
    }
}