package uk.co.tomek.dogs.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.given
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.tomek.dogs.presentation.model.DogsListItem
import uk.co.tomek.dogs.domain.Interactor
import uk.co.tomek.dogs.presentation.model.MainViewState

@ExperimentalCoroutinesApi
class MainViewModelTest {

    private lateinit var mainViewModel: MainViewModel
    private val dispatcher = TestCoroutineDispatcher()

    @Mock
    private lateinit var interactor: Interactor<MainViewState>

    // rule to prevent Method getMainLooper in android.os.Looper not mocked.
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mainViewModel = MainViewModel(interactor, dispatcher)
    }

    @Test
    fun verifyThatErrorIsPropagatedWhenLoadingFails() {
        runBlockingTest {
            // given
            val throwable = Exception("An exception")
            val errorState = MainViewState.Error(throwable)
            given(interactor.fetchData(null)).willReturn(errorState)

            // when
            mainViewModel.fetchDogs()

            // then
            assertEquals(errorState, mainViewModel.mainViewState.value)
        }
    }

    @Test
    fun verifyThatDataStateIsPropagated() {
        runBlockingTest {
            // given
            val dogs = listOf<DogsListItem>()
            val dataState = MainViewState.Data(dogs)
            given(interactor.fetchData(null)).willReturn(dataState)

            // when
            mainViewModel.fetchDogs()

            // then
            assertEquals(dataState, mainViewModel.mainViewState.value)
        }
    }
}