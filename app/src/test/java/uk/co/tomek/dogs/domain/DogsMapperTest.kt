package uk.co.tomek.dogs.domain

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import uk.co.tomek.dogs.presentation.model.DogsListItem

class DogsMapperTest {

    private lateinit var mapper: DogsMapper

    @Before
    fun setUp() {
        mapper = DogsMapper()
    }

    @ExperimentalStdlibApi
    @Test
    fun verifyDogsDataIsProperlyTransformed() {
        // given
        val dogBreed1 = "dogBreed1"
        val dogBreed2 = "dogBreed2"
        val dogBreed3 = "dogBreed3"
        val dogSubBreed11 = "dogSubBreed11"
        val dogSubBreed12 = "dogSubBreed12"
        val dogSubBreed21 = "dogSubBreed21"
        val mapOfDogs = mapOf(
            dogBreed1 to listOf(dogSubBreed11, dogSubBreed12),
            dogBreed2 to listOf(dogSubBreed21),
            dogBreed3 to emptyList()
        )
        val expected = listOf(
            DogsListItem(dogBreed1),
            DogsListItem(dogSubBreed11, true, dogBreed1),
            DogsListItem(dogSubBreed12, true, dogBreed1),
            DogsListItem(dogBreed2),
            DogsListItem(dogSubBreed21, true, dogBreed2),
            DogsListItem(dogBreed3)
        )

        // when
        val mappedDogsList = mapper.getFlatDogsList(mapOfDogs)

        // then
        assertEquals(expected, mappedDogsList)
    }
}