package uk.co.tomek.dogs.data

import com.squareup.moshi.JsonReader
import junit.framework.Assert
import okio.buffer
import okio.source
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class DogsResponseAdapterTest {

    private lateinit var adapter: DogsResponseAdapter

    @Before
    fun setUp() {
        adapter = DogsResponseAdapter()
    }

    @Test
    fun fromJson() {
        // given
        val inputStream = ClassLoader.getSystemResourceAsStream("get_dogs.json")
        val source = inputStream.source().buffer()
        val reader = JsonReader.of(source)

        // when
        val fromJson = adapter.fromJson(reader)

        // then
        assertTrue(fromJson?.message?.isEmpty() == false)
        val listOfAustralian = listOf("shepherd")
        assertEquals(listOfAustralian, fromJson?.message?.get("australian"))
    }
}