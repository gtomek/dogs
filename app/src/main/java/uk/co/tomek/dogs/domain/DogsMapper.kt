package uk.co.tomek.dogs.domain

import uk.co.tomek.dogs.presentation.model.DogsListItem

/**
 * Helper class used to map between different model types UI - Data.
 */
class DogsMapper {

    @ExperimentalStdlibApi
    fun getFlatDogsList(messages: Map<String, List<String>>): List<DogsListItem> {
        val items = mutableListOf<DogsListItem>()
        messages.entries.forEach { mapEntry ->
            items.add(DogsListItem(mapEntry.key))
            if (mapEntry.value.isNotEmpty()) {
                mapEntry.value.forEach { subBreed ->
                    items.add(
                        DogsListItem(
                            subBreed,
                            true,
                            mapEntry.key
                        )
                    )
                }
            }
        }
        return items
    }

}
