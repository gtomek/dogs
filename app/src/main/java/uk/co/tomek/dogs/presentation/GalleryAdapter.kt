package uk.co.tomek.dogs.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import uk.co.tomek.dogs.R

class GalleryAdapter : ListAdapter<String, GalleryAdapter.DogImageViewHolder>(GalleryDiffUtil())
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogImageViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_dog_image, parent, false)
        return DogImageViewHolder(view as ImageView)
    }

    override fun onBindViewHolder(holder: DogImageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class DogImageViewHolder(private val view: ImageView): RecyclerView.ViewHolder(view) {
        fun bind(item: String?) {
            view.load(item)
        }
    }

    class GalleryDiffUtil : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String) = oldItem == newItem
        override fun areContentsTheSame(oldItem: String, newItem: String) = oldItem == newItem
    }

}