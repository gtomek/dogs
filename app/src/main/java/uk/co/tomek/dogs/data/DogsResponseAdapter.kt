package uk.co.tomek.dogs.data

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson
import uk.co.tomek.dogs.data.model.DogsResponse

class DogsResponseAdapter : JsonAdapter<DogsResponse>() {

    @FromJson
    override fun fromJson(reader: JsonReader): DogsResponse? {
        val dogsList = mutableMapOf<String, List<String>>()
        reader.beginObject()
        reader.nextName()
        reader.beginObject()
        if (reader.hasNext()) {
            while (reader.hasNext()) {
                val nextDogName = reader.nextName()
                val subBreedList = mutableListOf<String>()
                reader.beginArray()
                while (reader.hasNext()) {
                    subBreedList.add(reader.nextString())
                }
                reader.endArray()
                dogsList[nextDogName] = subBreedList
            }
        }
        reader.endObject()
        reader.nextName()
        val status = reader.nextString()
        reader.endObject()

        return DogsResponse(dogsList, status)
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: DogsResponse?) {
        TODO("Not implemented as not required")
    }
}