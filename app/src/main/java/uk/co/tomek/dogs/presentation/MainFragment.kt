package uk.co.tomek.dogs.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uk.co.tomek.dogs.R
import uk.co.tomek.dogs.databinding.FragmentMainBinding
import uk.co.tomek.dogs.presentation.DetailsFragment.Companion.ARG_DOG
import uk.co.tomek.dogs.presentation.model.DogsListItem
import uk.co.tomek.dogs.presentation.model.MainViewState
import uk.co.tomek.dogs.presentation.viewmodel.MainViewModel

class MainFragment : Fragment() {

    private lateinit var dogsListAdapter: DogsListAdapter
    private val mainViewModel: MainViewModel by viewModel()

    private var _binding: FragmentMainBinding? = null
    private val binding get() = requireNotNull(_binding)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dogsListAdapter = DogsListAdapter(::openDetails)
        binding.recyclerDogsListMain.apply {
            layoutManager = LinearLayoutManager(view.context)
            adapter = dogsListAdapter
        }

        binding.layoutIncludeError.buttonErrorLayoutTryAgain.setOnClickListener {
            mainViewModel.fetchDogs()
        }

        mainViewModel.mainViewState
            .onEach { renderState(it) }
            .launchIn(lifecycleScope)
    }

    private fun openDetails(dog: DogsListItem) {
        findNavController().navigate(
            R.id.details_dest,
            bundleOf(
                ARG_DOG to dog
            )
        )
    }

    private fun renderState(state: MainViewState) {
        Timber.v("Render state $state")
        when (state) {
            MainViewState.Loading -> showLoading()
            is MainViewState.Error -> showError(state.throwable)
            is MainViewState.Data -> showData(
                state.dogs
            )
        }
    }

    private fun showLoading() {
        binding.progressBarMain.isVisible = true
        binding.layoutIncludeError.layoutError.isVisible = false
        binding.recyclerDogsListMain.isVisible = false
    }

    private fun showError(throwable: Throwable?) {
        Timber.e(throwable, "Loading error!")
        binding.progressBarMain.isVisible = false
        binding.layoutIncludeError.layoutError.isVisible = true
        binding.recyclerDogsListMain.isVisible = false
    }

    private fun showData(dogs: List<DogsListItem>) {
        binding.progressBarMain.isVisible = false
        binding.layoutIncludeError.layoutError.isVisible = false
        binding.recyclerDogsListMain.isVisible = true
        dogsListAdapter.submitList(dogs)
    }
}