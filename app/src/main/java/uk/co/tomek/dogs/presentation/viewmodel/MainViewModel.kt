package uk.co.tomek.dogs.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import uk.co.tomek.dogs.domain.Interactor
import uk.co.tomek.dogs.presentation.model.MainViewState

class MainViewModel(
    private val mainInteractor: Interactor<MainViewState>,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _mainViewState = MutableStateFlow<MainViewState>(MainViewState.Loading)
    val mainViewState: StateFlow<MainViewState>
        get() = _mainViewState

    init {
        fetchDogs()
    }

    fun fetchDogs() {
        _mainViewState.value = MainViewState.Loading
        viewModelScope.launch(dispatcher) {
            val fetchState = mainInteractor.fetchData(null)
            _mainViewState.value = fetchState
        }
    }

}