package uk.co.tomek.dogs.data.model

data class DogsMap (
    val dogs : Map<String, List<String>>
)