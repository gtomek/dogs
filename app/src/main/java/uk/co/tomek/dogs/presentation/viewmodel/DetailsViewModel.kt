package uk.co.tomek.dogs.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import uk.co.tomek.dogs.domain.Interactor
import uk.co.tomek.dogs.presentation.model.DetailsViewState

class DetailsViewModel (
    private val interactor: Interactor<DetailsViewState>,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _detailsViewState = MutableStateFlow<DetailsViewState>(DetailsViewState.Loading)
    val detailsViewState: StateFlow<DetailsViewState>
        get() = _detailsViewState

    fun fetchDogPhotos(dogBreed: String) {
        _detailsViewState.value = DetailsViewState.Loading
        viewModelScope.launch(dispatcher) {
            val fetchState = interactor.fetchData(dogBreed)
            _detailsViewState.value = fetchState
        }
    }

}