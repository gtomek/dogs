package uk.co.tomek.dogs.presentation.model

sealed class DetailsViewState {
    object Loading : DetailsViewState()
    data class Error(val throwable: Throwable? = null) : DetailsViewState()
    data class Data(
        val dogPhotoUrls: List<String>
    ) : DetailsViewState()
}