package uk.co.tomek.dogs.presentation.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DogsListItem(
    val breedName : String,
    val isSubBreed: Boolean = false,
    val rootBreedName : String? = null
) : Parcelable