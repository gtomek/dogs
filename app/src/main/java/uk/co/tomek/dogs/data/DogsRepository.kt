package uk.co.tomek.dogs.data

import uk.co.tomek.dogs.data.api.DogsService

class DogsRepository(private val netSource: DogsService) {

    suspend fun fetchDogs() = netSource.getDogs()

    suspend fun fetchPhotos(
        dogType: String,
        imagesNumber: Int
    ) = netSource.getDogImages(dogType, imagesNumber)
}