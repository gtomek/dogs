package uk.co.tomek.dogs.di

import android.content.Context
import com.readystatesoftware.chuck.ChuckInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import uk.co.tomek.dogs.BuildConfig
import uk.co.tomek.dogs.data.DogsRepository
import uk.co.tomek.dogs.data.api.DogsService
import uk.co.tomek.dogs.data.DogsResponseAdapter
import uk.co.tomek.dogs.domain.DetailsInteractor
import uk.co.tomek.dogs.domain.DogsMapper
import uk.co.tomek.dogs.presentation.viewmodel.DetailsViewModel
import uk.co.tomek.dogs.domain.Interactor
import uk.co.tomek.dogs.domain.MainInteractor
import uk.co.tomek.dogs.presentation.model.DetailsViewState
import uk.co.tomek.dogs.presentation.model.MainViewState
import uk.co.tomek.dogs.presentation.viewmodel.MainViewModel
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager

/**
 * KOIN modules declarations.
 */
val applicationModule: Module = module {

    factory {
        DogsRepository(
            get()
        )
    }

    factory {
        DogsMapper()
    }

    factory<Interactor<MainViewState>>(named("main")) {
        MainInteractor(
            get(),
            get()
        )
    }

    factory<Interactor<DetailsViewState>>(named("details")) {
        DetailsInteractor(
            get()
        )
    }
    viewModel { MainViewModel(get(named("main"))) }
    viewModel { DetailsViewModel(get(named("details"))) }
}

val networkModule: Module = module {
    single {
        createOkHttpClient(
            get()
        )
    }
    single {
        creteNetService<DogsService>(
            get(),
            BuildConfig.SERVER_URL
        )
    }
}

fun createOkHttpClient(
    context: Context,
    socketFactory: SSLSocketFactory? = null,
    trustManager: X509TrustManager? = null
): OkHttpClient {
    val logInterceptor = HttpLoggingInterceptor()
    logInterceptor.level = HttpLoggingInterceptor.Level.BODY
    val builder = OkHttpClient.Builder()
        .addInterceptor(logInterceptor)
        .addInterceptor(ChuckInterceptor(context))
    if (socketFactory != null && trustManager != null) {
        builder.sslSocketFactory(socketFactory, trustManager)
    }
    return builder.build()
}

inline fun <reified T> creteNetService(httpClient: OkHttpClient, baseUrl: String): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(httpClient)
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .add(DogsResponseAdapter())
                    .build()
            )
        )
        .build()
    return retrofit.create(T::class.java)
}