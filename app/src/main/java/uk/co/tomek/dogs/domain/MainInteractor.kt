package uk.co.tomek.dogs.domain

import uk.co.tomek.dogs.data.DogsRepository
import uk.co.tomek.dogs.domain.DogsMapper
import uk.co.tomek.dogs.domain.Interactor
import uk.co.tomek.dogs.presentation.model.MainViewState

/**
 * Main Interactor/use case for the main sitting between presentation (main Activity)
 * and data layer (Repository)
 */
class MainInteractor(
    private val repository: DogsRepository,
    private val mapper: DogsMapper
) : Interactor<MainViewState> {

    @ExperimentalStdlibApi
    override suspend fun fetchData(paramter: String?): MainViewState {
        return runCatching {
            val dogsResponse = repository.fetchDogs()
            MainViewState.Data(mapper.getFlatDogsList(dogsResponse.message))
        }
            .onFailure {
                MainViewState.Error(it)
            }
            .getOrDefault(MainViewState.Loading)
    }

}