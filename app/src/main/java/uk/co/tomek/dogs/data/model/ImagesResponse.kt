package uk.co.tomek.dogs.data.model

data class ImagesResponse(
    val message: List<String>,
    val status: String
)