package uk.co.tomek.dogs.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber
import uk.co.tomek.dogs.R
import uk.co.tomek.dogs.presentation.model.DogsListItem

class DogsListAdapter(
    private val clickListener: (DogsListItem) -> Unit
) : ListAdapter<DogsListItem, DogsListAdapter.DogItemViewHolder>(DogsDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        val titleView = view.findViewById<TextView>(R.id.textview_main_breed_title)
        val viewHolder = DogItemViewHolder(view, titleView)
        view.setOnClickListener {
            val position = viewHolder.adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                val dog = getItem(viewHolder.adapterPosition)
                clickListener(dog)
            }
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: DogItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position).isSubBreed) {
            R.layout.item_dogs_list_subbreed
        } else {
            R.layout.item_dogs_list_breed
        }
    }

    class DogItemViewHolder(
        view: View,
        private val titleView: TextView
    ) :
        RecyclerView.ViewHolder(view) {
        fun bind(item: DogsListItem) {
            Timber.v("Bind item $item")
            titleView.text = item.breedName.replaceFirstChar { it.uppercase() }
        }

    }
}