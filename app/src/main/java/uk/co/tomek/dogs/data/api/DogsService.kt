package uk.co.tomek.dogs.data.api

import retrofit2.http.GET
import retrofit2.http.Path
import uk.co.tomek.dogs.data.model.DogsResponse
import uk.co.tomek.dogs.data.model.ImagesResponse

interface DogsService {

    @GET("breeds/list/all")
    suspend fun getDogs(): DogsResponse

    @GET("breed/{dogType}/images/random/{imagesNumber}")
    suspend fun getDogImages(
        @Path("dogType") dogType: String,
        @Path("imagesNumber") imagesNumber: Int
    ) : ImagesResponse
}