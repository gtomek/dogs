package uk.co.tomek.dogs.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uk.co.tomek.dogs.databinding.FragmentDetailsBinding
import uk.co.tomek.dogs.presentation.model.DogsListItem
import uk.co.tomek.dogs.presentation.viewmodel.DetailsViewModel
import uk.co.tomek.dogs.presentation.model.DetailsViewState

class DetailsFragment : Fragment() {

    private lateinit var dogsGalleryAdapter: GalleryAdapter
    private val viewModel: DetailsViewModel by viewModel()

    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = requireNotNull(_binding)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dogsGalleryAdapter = GalleryAdapter()
        binding.recyclerDogsListDetails.apply {
            layoutManager =
                StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL).apply {
                    gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
                }
            adapter = dogsGalleryAdapter
        }

        arguments?.let { args ->
            val dog = args.getParcelable<DogsListItem>(ARG_DOG)
            dog?.let { dogBreedName ->
                // set title
                (requireActivity() as MainActivity).supportActionBar?.title =
                    "${dog.breedName.replaceFirstChar { it.uppercase() }} " +
                            "${dog.rootBreedName?.replaceFirstChar { it.uppercase() } ?: ""} "

                binding.layoutIncludeError.buttonErrorLayoutTryAgain.setOnClickListener {
                    viewModel.fetchDogPhotos(dogBreedName.breedName)
                }

                viewModel.fetchDogPhotos(dog.rootBreedName ?: dog.breedName)
            }
        }

        viewModel.detailsViewState
            .onEach { renderState(it) }
            .launchIn(lifecycleScope)
    }

    private fun renderState(state: DetailsViewState) {
        Timber.v("Render state $state")
        when (state) {
            DetailsViewState.Loading -> showLoading()
            is DetailsViewState.Error -> showError(state.throwable)
            is DetailsViewState.Data -> showData(
                state.dogPhotoUrls
            )
        }
    }

    private fun showData(dogs: List<String>) {
        binding.progressBarDetails.isVisible = false
        binding.layoutIncludeError.layoutError.isVisible = false
        binding.recyclerDogsListDetails.apply {
            isVisible = true
            dogsGalleryAdapter.submitList(dogs)
        }
    }

    private fun showError(throwable: Throwable?) {
        Timber.e(throwable, "Loading error!")
        binding.progressBarDetails.isVisible = false
        binding.layoutIncludeError.layoutError.isVisible = true
        binding.recyclerDogsListDetails.isVisible = false
    }

    private fun showLoading() {
        binding.progressBarDetails.isVisible = true
        binding.layoutIncludeError.layoutError.isVisible = false
        binding.recyclerDogsListDetails.isVisible = false
    }

    companion object {
        const val ARG_DOG = "arg_dog"
    }
}