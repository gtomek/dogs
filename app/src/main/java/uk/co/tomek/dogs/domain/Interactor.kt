package uk.co.tomek.dogs.domain

/**
 * Interactor/Use case abstraction used to interact between data and presentation layers
 */
interface Interactor<T> {
    suspend fun fetchData(paramter: String? = null): T
}