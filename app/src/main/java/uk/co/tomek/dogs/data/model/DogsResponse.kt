package uk.co.tomek.dogs.data.model

data class DogsResponse(
    val message: Map<String, List<String>>,
    val status: String
)

