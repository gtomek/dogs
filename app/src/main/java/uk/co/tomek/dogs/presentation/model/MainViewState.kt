package uk.co.tomek.dogs.presentation.model

sealed class MainViewState {
    object Loading : MainViewState()
    data class Error(val throwable: Throwable? = null) : MainViewState()
    data class Data(
        val dogs: List<DogsListItem>
    ) : MainViewState()
}