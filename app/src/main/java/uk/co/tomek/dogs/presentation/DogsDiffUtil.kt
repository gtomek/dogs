package uk.co.tomek.dogs.presentation

import androidx.recyclerview.widget.DiffUtil
import uk.co.tomek.dogs.presentation.model.DogsListItem

/**
 * Simplified version of diff utils.
 */
class DogsDiffUtil : DiffUtil.ItemCallback<DogsListItem>() {

    override fun areContentsTheSame(oldItem: DogsListItem, newItem: DogsListItem): Boolean =
        oldItem.breedName == newItem.breedName &&
                oldItem.isSubBreed == newItem.isSubBreed


    override fun areItemsTheSame(oldItem: DogsListItem, newItem: DogsListItem): Boolean =
        oldItem == newItem
}