package uk.co.tomek.dogs.domain

import timber.log.Timber
import uk.co.tomek.dogs.data.DogsRepository
import uk.co.tomek.dogs.presentation.model.DetailsViewState

class DetailsInteractor(
    private val repository: DogsRepository
) : Interactor<DetailsViewState> {

    override suspend fun fetchData(paramter: String?): DetailsViewState {
        return try {
            paramter?.let { breedToFetch ->
                Timber.v("Requesting dog photos for $breedToFetch")
                val dogsResponse = repository.fetchPhotos(breedToFetch,
                    IMAGES_NUMBER)
                DetailsViewState.Data(dogsResponse.message)
            } ?: DetailsViewState.Error()

        } catch (exception: Exception) {
            DetailsViewState.Error(exception)
        }
    }

    companion object {
        const val IMAGES_NUMBER = 20
    }
}