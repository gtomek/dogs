// inspired by:
// https://antonioleiva.com/kotlin-dsl-gradle/
// https://proandroiddev.com/migrate-to-gradle-kotlin-dsl-in-4-steps-f3e3b27e1f4d

object Config {
    const val kotlinVersion = "1.5.10"

    object Android {
        const val buildToolsVersion = "30.0.3"
        const val minSdkVersion = 23
        const val compileSdkVersion = 30
        const val targetSdkVersion = 30
    }

    object App {
        const val versionCode = 1
        const val versionName = "1.0.0"
    }
}

object Libraries {
    private object Versions {
        const val koin = "2.2.2"
        const val leakCanary = "2.3"
        const val timber = "4.7.1"
        const val navigationKtx = "2.3.5"
        const val fragmentX = "1.2.2"
        const val appCompat = "1.3.0"
        const val androidxCore = "1.5.0"
        const val kotlinCoroutines = "1.5.0"
        const val constraintlayout = "2.0.4"
        const val material = "1.3.0"
        const val drawerLayout = "1.0.0"
        const val lifecycleSaveState = "2.2.0"
        const val retrofit = "2.9.0"
        const val moshi = "1.9.2"
        const val okHttp = "4.9.1"
        const val biometric = "1.0.1"
        const val chuck = "1.1.0"
        const val coil = "1.2.1"
    }

    // Android X
    const val androidxAppCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val androidxCoreKtx = "androidx.core:core-ktx:${Versions.androidxCore}"
    const val androidxConstraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintlayout}"
    const val androidxFragment = "androidx.fragment:fragment:${Versions.fragmentX}"
    const val androidxNavigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigationKtx}"
    const val androidxNavigationUiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigationKtx}"
    const val androidxDrawerLayout = "androidx.drawerlayout:drawerlayout:${Versions.drawerLayout}"
    const val androidxLifecycleSaveState =
        "androidx.lifecycle:lifecycle-viewmodel-savedstate:${Versions.lifecycleSaveState}"
    const val androidxAnnotation = "androidx.annotation:annotation:${Versions.appCompat}"
    const val androidxBiometric = "androidx.biometric:biometric:${Versions.biometric}"

    // coroutines
    const val kotlinCoroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinCoroutines}"
    const val kotlinCoroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.kotlinCoroutines}"

    // dependency injection
    const val koinCore = "org.koin:koin-core:${Versions.koin}"
    const val koinAndroid = "org.koin:koin-android:${Versions.koin}"
    const val koinAndroidViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"

    // others
    const val material ="com.google.android.material:material:${Versions.material}"
    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leakCanary}"
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

    // images loading
    const val coilImagesLoader = "io.coil-kt:coil:${Versions.coil}"

    // network
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitConverterMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val okHttp = "com.squareup.okhttp3:okhttp:${Versions.okHttp}"
    const val okHttpTls = "com.squareup.okhttp3:okhttp-tls:${Versions.okHttp}"
    const val okHttpUrlConnection = "com.squareup.okhttp3:okhttp-urlconnection:${Versions.okHttp}"
    const val okHttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    const val okHttpMockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.okHttp}"
    const val moshi = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    const val moshiAdapters = "com.squareup.moshi:moshi-adapters:${Versions.moshi}"
    const val chuckDebug = "com.readystatesoftware.chuck:library:${Versions.chuck}"
    const val chuckRelease = "com.readystatesoftware.chuck:library-no-op:${Versions.chuck}"
    // if reflection is not welcome, use Moshi CodeGen
    const val moshiCodegen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"
    const val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:${Config.kotlinVersion}"
}

object TestLibraries {
    private object Versions {
        const val junit4 = "4.13"
        const val testRunner = "1.3.0"
        const val espressoCore = "3.3.0"
        const val mockitoCore = "3.3.3"
        const val mockitoKotlin = "2.2.0"
        const val androidXTestJunit = "1.1.1"
        const val coroutinesTest = "1.3.2"
        const val androidXCoreTesting = "2.1.0"
        const val fragmentX = "1.2.2"
        const val restMockServer = "0.4.3"
        const val testRulesX = "1.3.0"
    }

    const val junit4 = "junit:junit:${Versions.junit4}"
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
    const val mockitoCore = "org.mockito:mockito-core:${Versions.mockitoCore}"
    const val mockitoInline = "org.mockito:mockito-inline:${Versions.mockitoCore}" // for mocking final classes
    const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKotlin}"
    const val androidXTestJunit = "androidx.test.ext:junit:${Versions.androidXTestJunit}"
    const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesTest}"
    const val androidXCoreTesting = "androidx.arch.core:core-testing:${Versions.androidXCoreTesting}"
    const val androidXFragmentTesting = "androidx.fragment:fragment-testing:${Versions.fragmentX}"
    const val restMockServer = "com.github.andrzejchm.RESTMock:android:${Versions.restMockServer}"
    const val androidXTestRules = "androidx.test:rules:${Versions.testRulesX}"
}